﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mesozoic;

namespace MesozoicSolution
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Donner naissance à un dinosaure : ");
            Diplodocus dinosaur1 = new Diplodocus("", 0);
            dinosaur1.setName();
            dinosaur1.setSpecie();
            dinosaur1.setAge();

            Console.WriteLine(dinosaur1.hug(dinosaur1));
            Console.WriteLine(dinosaur1.sayHello());
            Console.WriteLine(dinosaur1.roar());
            Console.ReadKey();
        }
    }
}
