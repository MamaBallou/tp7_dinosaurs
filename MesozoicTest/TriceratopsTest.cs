﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mesozoic;

namespace MesozoicTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestDinosaurConstructor()
        {
            Triceratops louis = new Triceratops("Louis", 12);

            Assert.AreEqual("Louis", louis.name);
            Assert.AreEqual("Triceratops", Triceratops.specie);
            Assert.AreEqual(12, louis.age);
        }

        [TestMethod]
        public void TestDinosaurRoar()
        {
            Triceratops louis = new Triceratops("Louis", 12);
            Assert.AreEqual("Zbraah", louis.roar());
        }

        [TestMethod]
        public void TestDinosaurSayHello()
        {
            Triceratops louis = new Triceratops("Louis", 12);
            Assert.AreEqual("Je suis un Triceratops de 12 ans et je m'appelle Louis.", louis.sayHello());
        }

        [TestMethod]
        public void TestDinosaurHug()
        {
            Triceratops louis = new Triceratops("Louis", 12);
            Triceratops nessie = new Triceratops("Nessie", 11);
            Assert.AreEqual("Je suis Louis et je fais un calin à Nessie.", louis.hug(nessie));
        }

        [TestMethod]
        public void TestDinosaurGetName()
        {
            Triceratops louis = new Triceratops("Louis", 12);
            Assert.AreEqual("Louis", louis.getName());
        }

        [TestMethod]
        public void TestDinosaurGetAge()
        {
            Triceratops louis = new Triceratops("Louis", 12);
            Assert.AreEqual(12, louis.getAge());
        }

        [TestMethod]
        public void TestDinosaurGetSpecie()
        {
            Triceratops louis = new Triceratops("Louis", 12);
            Assert.AreEqual("Triceratops", louis.getSpecie());
        }
    }
}
