﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mesozoic;

namespace MesozoicTest
{
    [TestClass]
    public class DiplodocusTest
    {
        [TestMethod]
        public void TestDinosaurConstructor()
        {
            Diplodocus louis = new Diplodocus("Louis", 12);

            Assert.AreEqual("Louis", louis.name);
            Assert.AreEqual("Diplodocus", Diplodocus.specie);
            Assert.AreEqual(12, louis.age);
        }

        [TestMethod]
        public void TestDinosaurRoar()
        {
            Diplodocus louis = new Diplodocus("Louis", 12);
            Assert.AreEqual("Bru", louis.roar());
        }

        [TestMethod]
        public void TestDinosaurSayHello()
        {
            Diplodocus louis = new Diplodocus("Louis", 12);
            Assert.AreEqual("Je suis Louis un Diplodocus, je viens d'avoir 12 ans.", louis.sayHello());
        }

        [TestMethod]
        public void TestDinosaurHug()
        {
            Diplodocus louis = new Diplodocus("Louis", 12);
            Diplodocus nessie = new Diplodocus("Nessie", 11);
            Assert.AreEqual("Je suis Louis et je fais un calin à Nessie.", louis.hug(nessie));
        }

        [TestMethod]
        public void TestDinosaurGetName()
        {
            Diplodocus louis = new Diplodocus("Louis", 12);
            Assert.AreEqual("Louis", louis.getName());
        }

        [TestMethod]
        public void TestDinosaurGetAge()
        {
            Diplodocus louis = new Diplodocus("Louis", 12);
            Assert.AreEqual(12, louis.getAge());
        }

        [TestMethod]
        public void TestDinosaurGetSpecie()
        {
            Diplodocus louis = new Diplodocus("Louis", 12);
            Assert.AreEqual("Diplodocus", louis.getSpecie());
        }
    }
}
