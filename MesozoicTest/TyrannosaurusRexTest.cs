﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mesozoic;

namespace MesozoicTest
{
    [TestClass]
    public class TyrannosaurusRexTest
    {
        [TestMethod]
        public void TestDinosaurConstructor()
        {
            TyrannosaurusRex louis = new TyrannosaurusRex("Louis", 12);

            Assert.AreEqual("Louis", louis.name);
            Assert.AreEqual("TyrannosaurusRex", TyrannosaurusRex.specie);
            Assert.AreEqual(12, louis.age);
        }

        [TestMethod]
        public void TestDinosaurRoar()
        {
            TyrannosaurusRex louis = new TyrannosaurusRex("Louis", 12);
            Assert.AreEqual("Grooooooaaaaaarrrrr", louis.roar());
        }

        [TestMethod]
        public void TestDinosaurSayHello()
        {
            TyrannosaurusRex louis = new TyrannosaurusRex("Louis", 12);
            Assert.AreEqual("Tremblez de peur devant Louis le TyrannosaurusRex qui vous dévorera du haut de ses 12 ans.", louis.sayHello());
        }

        [TestMethod]
        public void TestDinosaurHug()
        {
            TyrannosaurusRex louis = new TyrannosaurusRex("Louis", 12);
            TyrannosaurusRex nessie = new TyrannosaurusRex("Nessie", 11);
            Assert.AreEqual("Je suis Louis et je fais un calin à Nessie.", louis.hug(nessie));
        }

        [TestMethod]
        public void TestDinosaurGetName()
        {
            TyrannosaurusRex louis = new TyrannosaurusRex("Louis", 12);
            Assert.AreEqual("Louis", louis.getName());
        }

        [TestMethod]
        public void TestDinosaurGetAge()
        {
            TyrannosaurusRex louis = new TyrannosaurusRex("Louis", 12);
            Assert.AreEqual(12, louis.getAge());
        }

        [TestMethod]
        public void TestDinosaurGetSpecie()
        {
            TyrannosaurusRex louis = new TyrannosaurusRex("Louis", 12);
            Assert.AreEqual("TyrannosaurusRex", louis.getSpecie());
        }
    }
}
