﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mesozoic;

namespace MesozoicTest
{
    [TestClass]
    public class StegausaurusTest
    {
        [TestMethod]
        public void TestDinosaurConstructor()
        {
            Stegausaurus louis = new Stegausaurus("Louis", 12);

            Assert.AreEqual("Louis", louis.name);
            Assert.AreEqual("Stegausaurus", Stegausaurus.specie);
            Assert.AreEqual(12, louis.age);
        }

        [TestMethod]
        public void TestDinosaurRoar()
        {
            Stegausaurus louis = new Stegausaurus("Louis", 12);
            Assert.AreEqual("Braa", louis.roar());
        }

        [TestMethod]
        public void TestDinosaurSayHello()
        {
            Stegausaurus louis = new Stegausaurus("Louis", 12);
            Assert.AreEqual("Louis, Stegausaurus, 12 ans, célibataire.", louis.sayHello());
        }

        [TestMethod]
        public void TestDinosaurHug()
        {
            Stegausaurus louis = new Stegausaurus("Louis", 12);
            Stegausaurus nessie = new Stegausaurus("Nessie", 11);
            Assert.AreEqual("Je suis Louis et je fais un calin à Nessie.", louis.hug(nessie));
        }

        [TestMethod]
        public void TestDinosaurGetName()
        {
            Stegausaurus louis = new Stegausaurus("Louis", 12);
            Assert.AreEqual("Louis", louis.getName());
        }

        [TestMethod]
        public void TestDinosaurGetAge()
        {
            Stegausaurus louis = new Stegausaurus("Louis", 12);
            Assert.AreEqual(12, louis.getAge());
        }

        [TestMethod]
        public void TestDinosaurGetSpecie()
        {
            Stegausaurus louis = new Stegausaurus("Louis", 12);
            Assert.AreEqual("Stegausaurus", louis.getSpecie());
        }
    }
}
